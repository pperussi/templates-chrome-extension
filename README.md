# Templates Extension

Project about a Google Chrome extension to add some basic e-mail templates to Gmail using InboxSDK. This project is a challenge for the software engineering vacancy at Amplemarket.

### Seting up local development

To run rails server:

```bash
docker-compose build .
docker-compose run rails rake db:setup
docker-compose up
```

To use chrome-extension project:

- Open Chrome to `chrome://extensions` and check the "Developer Mode" checkbox
- Click on "Load Unpacked Extension" and point it to `chrome-extension` directory.

##### Project Video

https://www.loom.com/share/32a266d1af6a4759b2ca1348cd769f7d
