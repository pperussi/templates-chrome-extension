var template

window.onmessage = function(event){
		message = event.data.get('template')
	if ( message !== null) {
		template = message;
		console.log(template)
	}
};

InboxSDK.load(2, 'sdk_templates_7981a4e7e9').then(function(sdk){
	sdk.Compose.registerComposeViewHandler(function(composeView){
		composeView.addButton({
			title: "Templates",
			iconUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSq3x0iIYhzn7yVPT6nyV3S1tMrHDDh7w4rvg&usqp=CAU',
			onClick: function(event) {
				sdk.Widgets.showModalView({
					title: 'Choose Your Template',
					el: '<iframe id="myframe" height="500" width="500" src="http://localhost:3000"></iframe>',
					chrome: true,
					buttons: [
						{
							text: 'Set Template',
							title: 'Set the chosen template in your e-mail body ',
							onClick: () => {
								event.composeView.setBodyHTML(template);
								sdk.Widgets.showModalView.close()
							},
						}
					],
				});
			},
		});
	});
});