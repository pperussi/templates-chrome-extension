# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Template.create(title: 'Contato do Cliente', content: '"<p>Olá [Primeiro-Nome],</p><p>Gostaríamos de agradecer o seu contato e informar que já estamos providenciando as informações solicitadas. Nosso time retornará assim que o orçamento estiver finalizado.</p><p>Atenciosamente, [Time].</p>"')

Template.create(title: 'Convite para Reunião', content: '"<p>Olá [Primeiro Nome],</p><p>Tudo bem com você?</p><p>Segue o link e o invite para a reunião que combinamos.</p><p><b>Data: [Data]]</b></p><p>Obrigada!</p>"')

Template.create(title: 'Aviso de Manutenção', content: '"<p>Prezados,</p><p>No dia [Data] faremos uma manutenção em nosso sistema. Portanto poderá apresentar instabilidade durante o período de atualização.</p><p>Qualquer dúvida podem entrar em contato conosco.</p><p>Atensiosamente, [Nome].</p>"') 